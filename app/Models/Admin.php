<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email',
//    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
    // حذف created _at updated at
    //public $timestamps = false;
    // نام جدول
    protected $table = 'admins';
    //انتخاب کانکشن
    #protected $connection = 'connection-name';

    protected $guarded = [];
    protected $guard = 'admins';

    // تبدیل به timestamp
    protected $dateFormat = 'U';

    // reletion ship 1:1
    public function phone()
    {
        return $this->hasOne('App\Model\Phone');
    }
    // reletion ship 1:n
    public function comments()
    {
        return $this->hasMany('App\Model\Comment');
    }


    // scope + join
    public function scopeList_users($query , $id)
    {
        return $query->join('phones', 'phones.user_id', '=', 'users.id')
            ->where('users.id','>', $id)
            ->get();
    }
    public function getRouteKeyName()
    {

        // جستجو از طریق این فیلد
        return 'id';
    }
}

