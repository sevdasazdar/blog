<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function images()
    {
        $style = "images.css";
        $pageTitle="دکتر علیرضا علمداری | عمل ترمیمی بینی| عکس عمل بینی| عکس قبل و بعد عمل بینی | بهترین دکتر زیبایی بینی | فلوشیپ جراحی زیبایی بینی و متخصص جراحی گوش و حلق و بینی";

        if (isset(request()->type)) {
            $images =  Image::where('type', request()->type)->whereNull('deleted_at')->orderBy('id','DESC')->get();
        } else {

            $images = Image::whereNull('deleted_at')->orderBy('id','DESC')->get();
        }
        return view('images', ['style' => $style,'pageTitle'=>$pageTitle, 'images' => $images]);
    }
}
