<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function index()
    {
       
        $data['video'] =  Video::whereNull('deleted_at')->latest('id')->get();
        $data['title'] = 'لیست ویدیوها';
        $data['url'] = '';
        $data['auth'] = Auth::guard('admin')->user();

        return view('admin.video.index', $data);
    }

    public function create()
    {
        $data['title'] = 'ثبت ویدیو';

        $data['url'] = '';
        $data['auth'] = Auth::guard('admin')->user();
        $url = "/images/poioi.jpg";
        return view('admin.video.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
            'video' => 'required|file|mimes:mp4,mov,MOV,ogv',
        ]);
        $videoName = $request->video->getClientOriginalName();
        $videoPath = 'videos/' . $videoName;
        if (!Storage::disk('public')->exists($videoPath)) {
            $isVideoUploaded = Storage::disk('public')->put($videoPath, file_get_contents($request->video));
        }

        $imageName = $request->image->getClientOriginalName();
        $imagePath = 'images/' . $imageName;

        if (!Storage::disk('public')->exists($imagePath)) {
            $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->image));
        }
        $existVideo = Video::where('video', $videoPath)->whereNull('deleted_at')->first();
        if (empty($existVideo)) {
            $video = new Video();
            $video->type = $request->type;
            $video->date = $request->date;
            $video->video = $videoPath;
            $video->image = $imagePath;
            $video->save();
            return back()->with('msg', '   ثبت در سیستم انجام شد .');
        }

        return back()
            ->with('msg', 'Unexpected error occured');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //http://127.0.0.1:8000/learn/resource/10
        // nemayesh taki
        return 'show' . $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'ویرایش ';

        $data['video'] = Video::findOrFail($id);
        $data['auth'] = Auth::guard('admin')->user();

        $data['url'] = '';
        return view('admin.video.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $videoSelected = Video::findOrfail($id);
        $this->validate($request, [
            'date' => 'required',
        ]);
        #file
        if ($request->hasFile('image')) {
            $image = 'images/' . $request->image->getClientOriginalName();
            $videoSelected->image = $image;
            if (!Storage::disk('public')->exists($image)) {
                $isimageUploaded = Storage::disk('public')->put($image, file_get_contents($request->image));
            }
        }
        if ($request->hasFile('video')) {
            $video = 'videos/' . $request->video->getClientOriginalName();
            $videoSelected->video = $video;
            if (!Storage::disk('public')->exists($video)) {
                $isimageUploaded = Storage::disk('public')->put($video, file_get_contents($request->video));
            }
        }

        $videoSelected->date = $request->date;
        $videoSelected->type = $request->type;

        $videoSelected->update();
        return back()->with('msg', '  تغییرات با موفقیت انجام شد  .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Video::destroy($id);
        return back()->with('msg', 'مخاطب حذف  شد.');
    }
}
