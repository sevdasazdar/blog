<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function index()
    {
        
        $data['image'] =  Image::whereNull('deleted_at')->latest('id')->get();
        $data['title'] = 'لیست عکسها';
        $data['url'] = '';
        $data['auth'] = Auth::guard('admin')->user();

        return view('admin.image.index', $data);
    }

    public function create()
    {
        $data['title'] = 'ثبت عکس';

        $data['url'] = '';
        $data['auth'] = Auth::guard('admin')->user();
        $url = "/images/poioi.jpg";
        return view('admin.image.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required',
            'tamamrokh' => 'required|file|mimes:png,jpeg,jpg',
            'nimrokh' => 'required|file|mimes:png,jpeg,jpg',

        ]);

        $nimrokhName = $request->nimrokh->getClientOriginalName();
        $nimrokhPath = 'pictures/' . $nimrokhName;
        if (!Storage::disk('public')->exists($nimrokhPath)) {
            $isnimrokhUploaded = Storage::disk('public')->put($nimrokhPath, file_get_contents($request->nimrokh));
        }


        $tamamrokhName = $request->tamamrokh->getClientOriginalName();
        $tamamrokhPath = 'pictures/' . $tamamrokhName;
        if (!Storage::disk('public')->exists($tamamrokhPath)) {
            $isTamamrokhUploaded = Storage::disk('public')->put($tamamrokhPath, file_get_contents($request->tamamrokh));
        }

        $existPic = Image::where('tamamrokh', $tamamrokhPath)->whereNull('deleted_at')->first();
        if (empty($existPic)) {
            $image = new Image();
            $image->type = $request->type;
            $image->date = $request->date;
            $image->tamamrokh = $tamamrokhPath;
            $image->nimrokh = $nimrokhPath;
            $image->save();

            return back()->with('msg', '   ثبت در سیستم انجام شد .');
        }

        return back()
            ->with('msg', 'Unexpected error occured');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //http://127.0.0.1:8000/learn/resource/10
        // nemayesh taki
        return 'show' . $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'ویرایش ';

        $data['image'] = Image::findOrFail($id);
        $data['auth'] = Auth::guard('admin')->user();

        $data['url'] = '';
        return view('admin.image.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = Image::findOrfail($id);
        #validate
        $this->validate($request, [
            'date' => 'required',
        ]);
        #file
        if ($request->hasFile('nimrokh')) {
            // dd($request->nimrokh->getClientOriginalName());
            $nimrokh = 'pictures/' . $request->nimrokh->getClientOriginalName();
            $image->nimrokh = $nimrokh;
            if (!Storage::disk('public')->exists($nimrokh)) {
                $isnimrokhUploaded = Storage::disk('public')->put($nimrokh, file_get_contents($request->nimrokh));
            }
        }
        if ($request->hasFile('tamamrokh')) {
            // dd($request->tamamrokh->getClientOriginalName());

            $tamamrokh = 'pictures/' . $request->tamamrokh->getClientOriginalName();
            $image->tamamrokh = $tamamrokh;
            if (!Storage::disk('public')->exists($tamamrokh)) {
                $istamamrokhUploaded = Storage::disk('public')->put($tamamrokh, file_get_contents($request->nimrokh));
            }
        }
        $image->date = $request->date;
        $image->type = $request->type;
        $image->update();
        return back()->with('msg', '  تغییرات با موفقیت انجام شد  .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Image::destroy($id);
        return back()->with('msg', 'مخاطب حذف  شد.');
    }
}
