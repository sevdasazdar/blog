<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index(){
        $data['userinfo']=Auth::guard('admin')->user();
        $data['title'] = 'داشبورد';
        $data['auth']=Auth::guard('admin')->user();
        $data['video'] = Video::count();
        $data['image'] = Image::count();
        return view('admin.dashboard.index',$data);
    }
}
