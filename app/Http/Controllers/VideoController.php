<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function videos()
    {
        $style = "videos.css";
        $pageTitle="دکتر علیرضا علمداری |ویدیوهای جراحی زیبایی بینی| ویدیو عمل بینی گوشتی | بهترین جراح زیبایی | فلوشیپ جراحی زیبایی بینی و متخصص جراحی گوش و حلق و بینی";

        if (isset(request()->type)) {
            $videos =  Video::where('type', request()->type)->whereNull('deleted_at')->orderBy('id','DESC')->get();
        } else {
            $videos = Video::whereNull('deleted_at')->orderBy('id','DESC')->get();
        }
        return view('videos', ['style' => $style,'pageTitle'=>$pageTitle, 'videos' => $videos]);
    }
}
