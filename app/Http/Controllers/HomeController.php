<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Video;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {

        $style = "style.css";
        $pageTitle="دکتر علیرضا علمداری | عمل بینی | بهترین جراح بینی | فلوشیپ جراحی زیبایی بینی و متخصص جراحی گوش و حلق و بینی";
        return view('Home', ['style' => $style,'pageTitle'=>$pageTitle]);
    }

    public function about()
    {
        $style = "About.css";
        $pageTitle="درباره دکتر علیرضا علمداری | عمل بینی | جراحی اندوسکوپی سینوس|عمل گوش| فلوشیپ زیبایی بینی";

        return view('About', ['style' => $style,'pageTitle'=>$pageTitle]);
    }

    public function contact()
    {
        $style = "Contact.css";
        $pageTitle="تماس با ما | دکتر علیرضا علمداری | هزینه جراحی بینی | دکتر متخصص گوش و حلق و بینی";
       return view('Contact', ['style' => $style,'pageTitle'=>$pageTitle]);
    }

    public function sitemap(){
        return response()->view('page1')->header('Content-Type', 'text/xml');
    }
}
