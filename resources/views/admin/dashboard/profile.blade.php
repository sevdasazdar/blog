@extends('admin.master')
@section('content')
<div class="row oakenwidgets" id="profile-page">
    <div class="col-md-12">
        <!--Panel-->
        <div class="panel panel-default" id="profile-page">
            <div class="panel-heading">
                <div class="panel-title">پروفایل<small> من </small>
                </div>

            </div>

            <div class="profile-header image-background-2">
                <!--Profile Header-->
                <div class="row">

                    <div class="col-md-3">
                        <div class="user-img">
                            {{--<img src="@if($user->avatar) {{$url}}/{{$user->avatar}} @else {{$url}}/users/avatar.jpg @endif" alt="User Image">--}}
                            <img src="{{$url}}/users/avatar.jpg" alt="User Image">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="info-block">
                            <h2> {{$user->name}}  <span class="label label-warning"> ادمین</span></h2>
                            <p> ثبت نام در {{jdate($user->created_at)->format('Y/m/d')}}
                            <p>مدیریت سیستم

                            </p>
                            <ul class="list-inline margin-bottom-10px">
                                {{--<li>--}}
                                    {{--<h3>344</h3>پست</li>--}}
                                {{--<li>--}}
                                    {{--<h3>101</h3>ویدئو</li>--}}
                                {{--<li>--}}
                                    {{--<h3>83</h3>دوستان</li>--}}
                                {{--<li>--}}
                                    {{--<h3>1256</h3>تصاویر</li>--}}
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!--/Profile Header-->

            <div class="panel-body profile">
                <div class="row">





                    <!--Right-Side-->
                    <div class="col-md-12">
                        <!-- tabs normal-->
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#one-normal" data-toggle="tab"> ویرایش <span class="label label-info"></span></a>
                            </li>
                            <li><a href="#two-normal" data-toggle="tab">تغییر رمز  <span class="label label-info"></span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="one-normal">
                                @include('admin.message')
                                <form method="post" action="doprofile" enctype="multipart/form-data" class="form-horizontal" data-parsley-validate>
                                    <fieldset>
                                        <!-- Text input-->
                                        @csrf

                                        <div class="form-group">
                                            <label class="col-md-2  control-label " >  ایمیل  </label>
                                            <div class="col-md-10 ui-sortable">
                                                <input readonly placeholder="اینجا بنویسید" class="form-control input-md" value="{{$user->email}}" type="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2  control-label " >  نام و نام خانوادگی  </label>
                                            <div class="col-md-10 ui-sortable">
                                                <input name="name" placeholder="اینجا بنویسید" class="form-control input-md" value="{{$user->name}}" type="text">
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="form-group ">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6 ui-sortable">
                                                <button type="submit"  class="btn btn-block btn-success">
                                                    <i class="fa fa-2x fa-check"> </i>
                                                    ثبت در سیستم
                                                </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>


                            </div>
                            <!--/tab-pane-1-->
                            <div class="tab-pane" id="two-normal">

                                <form method="post" action="password" enctype="multipart/form-data" class="form-horizontal" data-parsley-validate>
                                    <fieldset>
                                        <!-- Text input-->
                                        @csrf

                                        <div class="form-group">
                                            <label class="col-md-2  control-label " >     رمز فعلی  </label>
                                            <div class="col-md-10 ui-sortable">
                                                <input required name="pass" placeholder="اینجا بنویسید" class="form-control input-md"  type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2  control-label " >     رمز جدید  </label>
                                            <div class="col-md-10 ui-sortable">
                                                <input required name="newpass" placeholder="اینجا بنویسید" class="form-control input-md"  type="password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2  control-label " >  تکرار   رمز جدید  </label>
                                            <div class="col-md-10 ui-sortable">
                                                <input required name="rep" placeholder="اینجا بنویسید" class="form-control input-md"  type="password">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6 ui-sortable">
                                                <button type="submit"  class="btn btn-block btn-success">
                                                    <i class="fa fa-2x fa-check"> </i>
                                                    ثبت در سیستم
                                                </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>

                            </div>
                            <!--/tab-pane-2-->
                            <div class="tab-pane" id="three-normal">
                            </div>
                            <!--/tab-pane-3-->
                        </div>
                        <!-- /tabs -->
                    </div>
                    <!--/col-md-9-->
                </div>
                <!--/Row-->
            </div>
            <!--/Panel Body-->
        </div>
        <!--/Panel-->
    </div>
    <!--/col-md-12-->
</div>
@endsection
