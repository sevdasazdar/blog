@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>
                        وظایف
                    </h2>
                </div>
                <div class="body">
                    <form action="{{route('send_chat')}}" method="post" class="row clearfix">
                        @csrf
                        <div class="col-lg-12 com-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select required class="select2" name="receiver_id">
                                        @foreach($users as $group)
                                            <option value="{{$group->id}}">{{$group->name}} {{$group->lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 com-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea required id="name" name="text" class="form-control"></textarea>
                                    <label class="form-label">نوشتن پیام</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="col-lg-8">
                                <button class="btn-hover btn-border-radius color-8">ارسال پیام</button>
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-12">
                        <div class="chat">
                            @foreach($chats as $item)
                                <div class="chatbox">
                                    <span class="chatname"> {{$item->receiver->name}} {{$item->receiver->lastname}}</span>
                                    <span class="chatdate">{{jdate('d F Y H:i',$item->created_at->timestamp)}}</span>
                                    <p class="chattext">{{$item->text}}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row" style="display: block">
                        <div class="col-lg-12 text-center pg">
                            {!! $pages !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@section('js')
    <style>
        .chat{
            float: right;
            width: 100%;
            min-height: 100px;
            margin-top: 15px;
        }
        .chatbox{
            float:right;
            width:100%;
            padding: 20px;
            margin-bottom: 15px;
            border-radius:5px;
            background-color: #eee;
        }
        .chatname{
            font-size: 15px;
            font-weight: bold;
            color: #388def;
        }
        .chatdate{
            font-size: 12px;
            color: #999;
            margin-bottom: 10px;
            float: right;
            width: 100%;
        }
        .chattext{
            margin: 0;
        }
    </style>

    {{--select2--}}
    <link rel="stylesheet" href="{{ $url }}/assets/admin/select2/select2.min.css">
    <script src="{{ $url }}/assets/admin/select2/select2.min.js"></script>
    <script>
        $('.select2').select2({ width: '100%' });
    </script>




    <style>
        .pg nav{
            background-color: #ffffff !important;
            box-shadow: 0 2px 2px 0 rgba(255, 255, 255, 0.14), 0 3px 1px -2px rgb(255, 255, 255), 0 1px 5px 0 rgba(0, 0, 0, 0.0);
            margin: 15px;
        }
        .pg ul{
            display: inline-block;
        }
        .page-item span{
            margin-top: 14px;
        }
    </style>
@endsection
