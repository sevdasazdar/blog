@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="profile-tab-box">
                    <div class="p-l-20">
                        <ul class="nav ">
                            <li class="nav-item tab-all">
                                <a class="nav-link active show" href="#me" data-toggle="tab">درباره من</a>
                            </li>
                            <li class="nav-item tab-all p-l-20">
                                <a class="nav-link " href="#pass" data-toggle="tab">رمز</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane  active show" id="me" aria-expanded="true">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>ویرایش  </strong> پروفایل</h2>
                        </div>
                        <form action="doprofile" method="post" class="body">
                            @csrf
                            <div class="form-group focused">
                                <input readonly type="text" class="form-control" placeholder=" " value="{{ $auth->username }}">
                            </div>
                            <div class="form-group ">
                                <input required name="name" type="text" class="form-control" placeholder="نام"  value="{{ $auth->name.' '.$auth->lastname }}">
                            </div>
                            <div class="form-group ">
                                <input required name="mobile" type="text" class="form-control" placeholder="موبایل"  value="{{ $auth->phone }}">
                            </div>

                            <button class="btn btn-info btn-round">ذخیره تغییرات</button>
                        </form>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="pass" aria-expanded="false">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>تنظیمات</strong> امنیتی</h2>
                        </div>
                        <form method="post" action="password" class="body">
                            @csrf
                            <div class="form-group focused">
                                <input required type="password" name="pass" class="form-control" placeholder="رمز عبور فعلی">
                            </div>
                            <div class="form-group">
                                <input required name="newpass" type="password" class="form-control" placeholder="رمز عبور جدید">
                            </div>
                            <div class="form-group">
                                <input required name="rep" type="password" class="form-control" placeholder="تکرار">
                            </div>
                            <button type="submit" class="btn btn-info btn-round"> تغییر رمز</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
