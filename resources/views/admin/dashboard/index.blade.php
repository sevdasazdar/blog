@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3 col-xs-4">
                            <img width="100" class="img-responsive img-circle" src="/assets/admin/images/user-orange.png" alt="User">
                        </div>
                        <div class="col-sm-9 col-xs-8">
                            <h3 class="margin-top-0"> {{$userinfo->name}} </h3>
                            <ul class="list-unstyled">
                                <li><strong> نام کاربری :</strong> {{$userinfo->email}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group" role="group">
                            <a href="/admin/profile" class="btn btn-info"><i class="dripicons-user"></i> ویرایش پروفایل</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class="padding-20px bg-violet border-radius-xs margin-bottom-20px">
                <div class="row">
                    <div class="col-xs-5">
                        <i class="fa fa-user-md fa-4x"></i>
                    </div>
                    <div class="col-xs-7">
                        <h5 class="margin-top-0 margin-bottom-5px">عکسها </h5>
                        <span class="text-xl">{{$image}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="padding-20px bg-violet border-radius-xs margin-bottom-20px">
                <div class="row">
                    <div class="col-xs-5">
                        <i class="fa fa-blind fa-4x"></i>
                    </div>
                    <div class="col-xs-7">
                        <h5 class="margin-top-0 margin-bottom-5px">ویدیوها </h5>
                        <span class="text-xl">{{$video}}</span>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default panel-filled">
            <div class="panel-body">
                <ul class="list-unstyled launcher-pad">
                    <li class="col-md-4 col-sm-4 col-xs-4"><a class="pop goaway " href="{{route('videos.index')}}"><i class="fa fa-user-md"></i>ویدیوها</a>
                    </li>
                    {{-- <li class="col-md-4 col-sm-4 col-xs-4"><a class="pop goaway " href="{{route('image.index')}}"><i class="fa fa-stethoscope"></i> عکسها</a> --}}
                    </li>
                    <li class="col-md-4 col-sm-4 col-xs-4"><a class="pop goaway " href="#"><i class="fa fa-power-off"></i>خروج</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

@endsection
