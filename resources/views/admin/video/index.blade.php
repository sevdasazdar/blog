@extends('admin.master')
@section('content')
    <!--Panel-->
    <div class="panel panel-default" id="tabledb3">
        <div class="panel-heading">
            <div class="panel-title">لیست <small>ویدیوها</small>
            </div>

        </div>
        <div class="panel-body">
            <form action="{{ route('videos.create') }}">
                <button class="btn btn-primary" type="submit"> create </button>
            </form>

            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="table-responsive">
                <table id="datatablesexample3" class="table table-striped">
                    <thead>
                        <tr>
                            <th> عکس </th>
                            <th> ویدیو </th>
                            <th> عملیات </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th> عکس </th>
                            <th> ویدیو </th>
                            <th> عملیات </th>
                        </tr>
                    </tfoot>

                    <tbody>
                        @foreach ($video as $item)
                            <tr>
                                <td><img src="{{ asset('storage/' . $item->image) }}" alt="video poster" style="width:20%">
                                </td>
                                <td>{{ $item->video ?? '' }}</td>
                                <td class="text-center">
                                    <a href="{{ route('videos.edit', $item->id) }}" data-toggle="tooltip"
                                        data-placement="right" data-original-title="ویرایش"
                                        class="fa fa-pencil  tooltiped text-warning"></a>
                                
                                    <form method="post" style="display: inline !important;"
                                        action="{{ route('videos.destroy', $item->id) }}">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <button type="button" data-toggle="tooltip" data-placement="right"
                                            data-original-title="حذف کن"
                                            class="fa fa-trash tooltiped text-danger btntable"></button>
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <!--/Panel Body-->
    </div>
    <!--/Panel-->

@endsection
@section('js')
    <!--Datatables-->
    <script type="text/javascript" src="/assets/admin/js/vendors/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/vendors/datatables/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="/assets/admin/js/vendors/datatables/dataTables.colReorder.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/vendors/datatables/datatables-examples.js"></script>
@endsection
