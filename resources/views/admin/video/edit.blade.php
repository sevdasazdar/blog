@extends('admin.master')
@section('content')
    <!--Panel-->
    <div class="panel panel-default" id="tabledb3">
        <div class="panel-heading">
            <div class="panel-title">ویرایش <small>پزشک</small>
            </div>

        </div>
        <div class="panel-body">
            <!-- Warning messages -->
            @include('admin.message')
            <form method="post" action="{{ route('videos.update', $video->id) }}" enctype="multipart/form-data"
                class="form-horizontal" data-parsley-validate>
                <fieldset>
                    <!-- Text input-->
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label class="col-md-2  control-label "> توضیحات</label>
                        <div class="col-md-10 ui-sortable">
                            <input required value="{{ $video->date }}" name="date" placeholder="اینجا بنویسید"
                                class="form-control input-md" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  control-label "> عکس </label>
                        <div class="col-md-10 ui-sortable">
                            <img src="{{ asset('storage/public/' . $video->image) }}" alt="عکس" style="width:10%">
                            <input type="file" name="image" class="form-control" value="{{($video->image)}}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  control-label "> ویدیو </label>
                        <div class="col-md-10 ui-sortable">
                            <input type="file" name="video" class="form-control" value="{{('storage/public/' .$video->video)}}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"> موضوع</label>
                        <div class="col-md-9 ui-sortable">
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-0-normal-styled" value="nose"
                                    @if ($video->type == 'nose') checked="checked" @endif type="radio">
                                <label for="radios-inline-0-normal-styled">بینی</label>
                            </div>
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-1-normal-styled" value="hear"
                                    @if ($video->type == 'ear') checked="checked" @endif type="radio">
                                <label for="radios-inline-1-normal-styled">گوش</label>
                            </div>
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-2-normal-styled" value="other"
                                    @if ($video->type == 'other') checked="checked" @endif type="radio">
                                <label for="radios-inline-2-normal-styled">دیگر</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 ui-sortable">
                            <button type="submit" class="btn btn-block btn-success">
                                <i class="fa fa-2x fa-check"> </i>
                                ثبت در سیستم
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>

        </div>
        <!--/Panel Body-->
    </div>
    <!--/Panel-->
@endsection
@section('js')
    <script src="/assets/admin/my.js"></script>
    <style>
        .control-label {
            text-align: right !important;
        }
    </style>
    {{-- upload --}}
    <link rel="stylesheet" href="/assets/admin/dropify/dist/css/dropify.min.css">
    <script src="/assets/admin/dropify/dist/js/dropify.min.js"></script>
    <script>
        $('.dropify').dropify({
            messages: {
                default: 'یک فایل اینجا بکشید و یا کلیک کنید',
                replace: 'برای جایگزینی ، یک فایل اینجا بکشید و یا کلیک کنید',
                remove: 'حذف',
                error: 'خطا در ارسال فایل',
            }
        });
    </script>

    {{-- validataion --}}
    <!--Parsley-->
    <script type="text/javascript" src="/assets/admin/js/vendors/parsley/parsley-config.js"></script>
    <script type="text/javascript" src="/assets/admin/js/vendors/parsley/parsley.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/vendors/parsley/comparison.js"></script>

    {{-- slect2 --}}
    <link rel="stylesheet" href="/assets/admin/select2/select2.min.css">
    <script src="/assets/admin/select2/select2.min.js"></script>
    <script>
        $('.select2').select2({
            width: '100%',
            dir: "rtl",
        });
    </script>
    <style>
        .select2-selection__rendered {
            background-color: #dee1e6;
        }
    </style>
@endsection
