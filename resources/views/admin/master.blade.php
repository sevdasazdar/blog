<!doctype html>
<html lang="ir">
<meta charset="UTF-8">
<meta http-equiv="content-type" content="text/html;charset=windows-1251">
<head>
    <title>{{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/assets/admin/images/logo-xs.png" type="image/png">
    <link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/dripicons.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/animate/animate.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/hover/hover-min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/styles.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/admin/css/btr.css" type="text/css">
    <link rel="stylesheet" href="/assets/admin/css/btr.css" type="text/css">
    @yield('css')
</head>

<body>
<div class="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top main-top-navbar">
        <a href="#" class="navbar-brand hidden-xs hidden-sm"><img alt="Admnistry Logo" src="/assets/admin/images/logo.png"></a>
        <a href="#" class="navbar-brand visible-xs visible-sm"><img alt="Admnistry Logo" src="/assets/admin/images/logo-xs.png"></a>
        <ul class="nav navbar-nav navbar-left">
            <li class="hidden-xs">
                <button class="sidebar-toggle btn" data-tooltip="tooltip" data-placement="bottom" title="سایدبار"><i class="dripicons-menu"></i></button>
            </li>
            <li class="visible-xs">
                <button class="sidebar-toggle-xs btn" data-tooltip="tooltip" data-placement="bottom" title="سایدبار"><i class="dripicons-menu"></i></button>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown user-menu">
                <a href="/assets/admin/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <img alt="User Image" class="img-circle" src="/assets/admin/images/user-orange.png"><span class="hidden-xs">{{$auth->name}}
                        <small>مدیریت</small></span></a>
                <ul class="dropdown-menu dropdown-menu-right launcher-pad" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <li class="col-md-4 col-sm-4 col-xs-4"><a class="pop goaway " href="{{route('videos.index')}}"><i class="fa fa-stethoscope"></i>ویدیو ها</a>
                    </li>
                    <li class="col-md-4 col-sm-4 col-xs-4"><a class="pop goaway " href="#"><i class="fa fa-power-off"></i>خروج</a>
                    </li>
                </ul>
            </li>
            <li class="hidden-xs">
                <button class="goaway btn text-red" data-tooltip="tooltip" data-placement="bottom" title="خروج"><i class="dripicons-exit"></i></button>
            </li>
            <li class="hidden-xs">
                <button class="lockme btn text-blue" data-tooltip="tooltip" data-placement="bottom" title="تغییر رمز و ویرایش پروفایل"><i class="dripicons-lock"></i></button>
            </li>
        </ul>
    </nav>
    <!--/Top Navigation-->

    <!-- Sidebar Wrapper-->
    <nav class="sidebar-wrapper navbar sidebar">
        <div class="sidebar-inner">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/admin/dashboard">
                        <i class="dripicons-home"></i>
                        <span class="li-text">پیشخوان</span>
                    </a>
                </li>
                
                <li>
                    <a href="#">
                        <i class="fa fa-user-md"> </i>
                        <span class="li-text">ویدیوها</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{route('videos.index')}}">
                                <span class="li-text">لیست ویدیوها</span>
                            </a>
                        </li>
                        
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-blind"> </i>
                        <span class="li-text">عکسها</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{route('images.index')}}">
                                <span class="li-text">لیست عکسها</span>
                            </a>
                        </li>
                        
                    </ul>
                </li>


                
                <li>
                    <a class="goaway" href="#">
                        <i class="dripicons-exit"></i>
                        <span class="li-text"> خروج</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div class="page-content-wrapper">
        @section('content')
            <h1>محتوایی جهت نمایش پیدا نشد .</h1>
        @show
    </div>
    <!--/Page Content -->

    <!--Bottom Bar-->
    <nav class="bottom-bar">

        <button class="btn scrollToTop pull-right" data-tooltip="tooltip" data-placement="top" title="بالا رفتن"><i class="dripicons-arrow-up"></i></button>
        <div class="btn-group dropup pull-right">
            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-tooltip="tooltip" data-placement="left" title="سبک"><i class="dripicons-brightness-medium"></i></button>
            <ul class="dropdown-menu change-style">
                <li class="animated bounceInLeft"><a href="/assets/admin/#" id="load-yellow-styles"><i class="fa fa-circle fa-3x text-yellow"></i></a></li>
                <li class="animated bounceInRight"><a href="/assets/admin/#" id="load-green-styles"><i class="fa fa-circle fa-3x text-green"></i></a></li>
                <li class="animated bounceInLeft"><a href="/assets/admin/#" id="load-khaki-styles"><i class="fa fa-circle fa-3x text-khaki"></i></a></li>
                <li class="animated bounceInRight"><a href="/assets/admin/#" id="load-blue-styles"><i class="fa fa-circle fa-3x text-blue"></i></a></li>
                <li class="animated bounceInRight"><a href="/assets/admin/#" id="load-pink-styles"><i class="fa fa-circle fa-3x text-pink"></i></a></li>
                <li class="animated bounceInLeft"><a href="/assets/admin/#" id="load-default-styles"><i class="fa fa-ban fa-3x"></i></a></li>
            </ul>
        </div>

        <!--Clear LocalStorage-->
        <button class="btn pull-right oaken-clear" data-tooltip="tooltip" data-placement="top" title="Clear Storage"><i class="dripicons-trash"></i></button>
        <!--/Clear LocalStorage-->
    </nav>
    <!--/Bottom Bar-->




</div>
<!-- /#wrapper -->



<!--Modals-->
<div class="modal modal-danger-filled" id="signout" data-easein="bounceIn" data-easeout="bounceOut">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body text-center padding-bottom-0">
                <i class="fa-5x dripicons-exit display-block"></i>
                <h5 class="margin-top-0">تمایل به خروج دارید؟</h5>
            </div>
            <div class="modal-footer text-center">
                <a href="/" type="button" class="btn btn-default" id="yesigo">بله</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">خیر</button>
            </div>
        </div>

    </div>
</div>
<!-- /.modal -->

<!--Lock Screen Dialog Modal-->
<div class="modal modal-info-filled" id="lockscreen" data-easein="bounceIn" data-easeout="bounceOut">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body text-center padding-bottom-0">
                <i class="fa-5x dripicons-lock display-block"></i>
                <h5 class="margin-top-0">برای انجام این کار مطمئن هستید ؟</h5>
            </div>
            <div class="modal-footer text-center">

                <a href="/admin/profile" type="button" class="btn btn-default" id="yesilock">بله</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">خیر</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!--Lock Screen Dialog Modal-->
<div class="modal modal-warning-filled" id="deletelinkmodal" data-easein="bounceIn" data-easeout="bounceOut">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body text-center padding-bottom-0">
                <i class="fa-5x fa fa-warning display-block"></i>
                <h5 class="margin-top-0">برای انجام این کار مطمئن هستید ؟</h5>
            </div>
            <div class="modal-footer text-center">

                <button type="button" class="btn btn-default" data-dismiss="modal">خیر</button>
                <button type="button" class="btn btn-default" id="yesido" >بله</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>

</div>
<!-- /.modal -->
<div class="modal modal-warning-filled" id="deletebtnmodal" data-easein="bounceIn" data-easeout="bounceOut">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body text-center padding-bottom-0">
                <i class="fa-5x fa fa-warning display-block"></i>
                <h5 class="margin-top-0">برای انجام این کار مطمئن هستید ؟</h5>
            </div>
            <div class="modal-footer text-center">

                <button type="button" class="btn btn-default" data-dismiss="modal">خیر</button>
                <button type="button" class="btn btn-default" id="yesido2" >بله</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<input type="hidden" value="" id="url">







<script type="text/javascript" src="/assets/admin/js/vendors/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/assets/admin/js/vendors/jquery/jquery-ui.min.js"></script>
<!--BootStrap-->
<script type="text/javascript" src="/assets/admin/js/vendors/bootstrap/bootstrap.min.js"></script>
<!--Widgets-->
<script type="text/javascript" src="/assets/admin/js/vendors/oakenwidgets/jquery.oakenwidgets.min.js"></script>
<!--Metis Menu-->
<script type="text/javascript" src="/assets/admin/js/vendors/metismenu/metisMenu.min.js"></script>
<!--Sparkline Charts-->
<script type="text/javascript" src="/assets/admin/js/vendors/sparkline/jquery.sparkline.min.js"></script>
<!--Nicescroller-->
<!--Modal Animation-->
<script type="text/javascript" src="/assets/admin/js/vendors/bootstrap-modal-animation/animation.js"></script>
<!--MomentJS-->
<script type="text/javascript" src="/assets/admin/js/vendors/moment/moment.min.js"></script>
<!--/Main - Used on every Administry page-->

<!--Main App-->
<script type="text/javascript" src="/assets/admin/js/scripts.js"></script>


{{--delete link alert--}}
<style>
    .deletelink{
        cursor: pointer;
        text-decoration: none;
    }
    .tooltiped:hover{
        text-decoration: none;
        font-size: 3em;

    }
    .tooltiped{
        font-size: 2em;
    }
    /*btntable*/
    .btntable{
        border: none;
        background-color: inherit;
    }
</style>
{{--deletelink--}}
<script>
    $(".deletelink").click(function (e) {
        var link = $(this).data('link');
        e.preventDefault(),
            $("#deletelinkmodal").modal(),
            $("#yesido").click(function () {
                window.open(link, "_self"), $("#lockscreen").modal("hide")
            })
    });
</script>
{{--delete btn--}}
<script>
    $(".btntable").click(function (e) {
        var form = $(this).parent();
        e.preventDefault(),
            $("#deletebtnmodal").modal(),
            $("#yesido2").click(function () {
                form.submit(), $("#lockscreen").modal("hide")
            })
    });
</script>

{{--alert messages :--}}
<script>
    @if (session('msg') or $errors->any())
    $(".alert-close").delay(6000).fadeOut("slow", function () {
        $(".alert-close").hide();
    });
    @endif
</script>
@yield('js')
</body>


</html>
