@extends('admin.master')
@section('content')
    <!--Panel-->
    <div class="panel panel-default" id="tabledb3">
        <div class="panel-heading">
            <div class="panel-title">ویرایش <small>عکس</small>
            </div>

        </div>
        <div class="panel-body">
            <!-- Warning messages -->
            @include('admin.message')
            <form method="post" action="{{ route('images.update', $image->id) }}" enctype="multipart/form-data"
                class="form-horizontal" data-parsley-validate>
                <fieldset>
                    <!-- Text input-->
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label class="col-md-2  control-label "> تاریخ عمل/توضیحات </label>
                        <div class="col-md-10 ui-sortable">
                            <input required value="{{ $image->date }}" name="date" placeholder="اینجا بنویسید"
                                class="form-control input-md" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  control-label "> عکس تمام رخ </label>
                        <div class="col-md-10 ui-sortable">
                            <img src="{{ asset('storage/public/' . $image->tamamrokh) }}" alt="عکس تمام رخ" style="width:10%">
                            <input type="file" name="tamamrokh" value="{{ $image->tamamrokh }}" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2  control-label "> عکس نیم رخ </label>
                        <div class="col-md-10 ui-sortable d-flex flex-row">
                            <img src="{{ asset('storage/public/' . $image->nimrokh) }}" alt="عکس نیم رخ"
                                style="width:10%;height:10%;">
                                {{-- @dd(asset($image->nimrokh)) --}}
                            <input type="file" name="nimrokh" value="{{asset($image->nimrokh) }}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"> موضوع</label>
                        <div class="col-md-9 ui-sortable">
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-0-normal-styled" value="nose"
                                    @if ($image->type == 'nose') checked="checked" @endif type="radio">
                                <label for="radios-inline-0-normal-styled">بینی</label>
                            </div>
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-1-normal-styled" value="hear"
                                    @if ($image->type == 'ear') checked="checked" @endif type="radio">
                                <label for="radios-inline-1-normal-styled">گوش</label>
                            </div>
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-2-normal-styled" value="other"
                                    @if ($image->type == 'other') checked="checked" @endif type="radio">
                                <label for="radios-inline-2-normal-styled">دیگر</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 ui-sortable">
                            <button type="submit" class="btn btn-block btn-success">
                                <i class="fa fa-2x fa-check"> </i>
                                ثبت در سیستم
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>

        </div>
        <!--/Panel Body-->
    </div>
    <!--/Panel-->
@endsection
@section('js')
    <script src="{{ $url }}/assets/admin/my.js"></script>
    <style>
        .control-label {
            text-align: right !important;
        }
    </style>
    {{-- upload --}}
    <link rel="stylesheet" href="{{ $url }}/assets/admin/dropify/dist/css/dropify.min.css">
    <script src="{{ $url }}/assets/admin/dropify/dist/js/dropify.min.js"></script>
    <script>
        $('.dropify').dropify({
            messages: {
                default: 'یک فایل اینجا بکشید و یا کلیک کنید',
                replace: 'برای جایگزینی ، یک فایل اینجا بکشید و یا کلیک کنید',
                remove: 'حذف',
                error: 'خطا در ارسال فایل',
            }
        });
    </script>

    {{-- validataion --}}
    <!--Parsley-->
    <script type="text/javascript" src="{{ $url }}/assets/admin/js/vendors/parsley/parsley-config.js"></script>
    <script type="text/javascript" src="{{ $url }}/assets/admin/js/vendors/parsley/parsley.min.js"></script>
    <script type="text/javascript" src="{{ $url }}/assets/admin/js/vendors/parsley/comparison.js"></script>

    {{-- slect2 --}}
    <link rel="stylesheet" href="{{ $url }}/assets/admin/select2/select2.min.css">
    <script src="{{ $url }}/assets/admin/select2/select2.min.js"></script>
    <script>
        $('.select2').select2({
            width: '100%',
            dir: "rtl",
        });
    </script>
    <style>
        .select2-selection__rendered {
            background-color: #dee1e6;
        }
    </style>
@endsection
