@extends('admin.master')
@section('content')
    <!--Panel-->
    <div class="panel panel-default" id="tabledb3">
        <div class="panel-heading">
            <div class="panel-title">ثبت <small>عکس</small>
            </div>
        </div>
        <div class="panel-body">
            <!-- Warning messages -->
            @include('admin.message')
            <form method="post" action="{{ route('images.store') }}" enctype="multipart/form-data" class="form-horizontal">
                @csrf

                <fieldset>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-2  control-label "> تاریخ عمل</label>
                        <div class="col-md-10 ui-sortable">
                            <input required name="date" placeholder="اینجا بنویسید" class="form-control input-md"
                                type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  control-label "> عکس تمام رخ </label>
                        <div class="col-md-10 ui-sortable">
                            <input type="file" name="tamamrokh" class="form-control" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  control-label "> عکس نیم رخ </label>
                        <div class="col-md-10 ui-sortable">
                            <input type="file" name="nimrokh" class="form-control" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"> موضوع</label>
                        <div class="col-md-9 ui-sortable">
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-0-normal-styled" value="nose" checked="checked"
                                    type="radio">
                                <label for="radios-inline-0-normal-styled">بینی</label>
                            </div>
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-1-normal-styled" value="ear" type="radio">
                                <label for="radios-inline-1-normal-styled">گوش</label>
                            </div>
                            <div class="radio-styled radio-inline">
                                <input name="type" id="radios-inline-2-normal-styled" value="other" type="radio">
                                <label for="radios-inline-2-normal-styled">دیگر</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 ui-sortable">
                            <button type="submit" class="btn btn-block btn-success">
                                <i class="fa fa-2x fa-check"> </i>
                                ثبت در سیستم
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <!--/Panel Body-->
    </div>
    <!--/Panel-->
@endsection
@section('js')
    <script src="/assets/admin/my.js"></script>
    <style>
        .control-label {
            text-align: right !important;
        }
    </style>

    <script type="text/javascript" src="/assets/admin/js/vendors/parsley/parsley-config.js"></script>
    <script type="text/javascript" src="/assets/admin/js/vendors/parsley/parsley.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/vendors/parsley/comparison.js"></script>
@endsection
