@if (session('msg') )
    <div class="callout callout-info alert-close">
        <h4>موفقیت آمیز</h4>
        <p>{{session('msg')}}</p>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-warning alert-close margin-top-20px">
        @foreach ($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif