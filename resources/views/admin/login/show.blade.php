<!doctype html>
<html lang="en">
<meta charset="utf-8">

<meta http-equiv="content-type" content="text/html;charset=windows-1251">
<head>

    <title> ورود به پنل مدیریت </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="/assets/admin/images/logo-xs.png" type="image/png">
    <link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/dripicons.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/animate/animate.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/hover/hover-min.css" rel="stylesheet" type="text/css">
    <link href="/assets/admin/css/styles.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/admin/css/btr.css" type="text/css">
</head>

<body>
<div class="standalone-page-wrapper image-background-1">
    <div class="center-wrapper">
        <div class="login-block">
            <div class="login-header text-center">
                <img src="/assets/admin/images/logo-xs.png" alt="Logo">
            </div>


            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" role="alert">
                        <strong>خطا</strong>
                        <br>{{ $error }}
                    </div>
                @endforeach
            @endif


            <form action="" method="post" class="form-horizontal" data-parsley-validate>
                <div class="form-group">
                    <label for="inputEmail3" class="col-md-3 control-label">ایمیل</label>
                    <div class="col-md-9">
                        <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="ایمیل" required>
                    </div>
                </div>
                @csrf
                <div class="form-group">
                    <label for="inputPassword3" class="col-md-3 control-label">پسورد</label>
                    <div class="col-md-9">
                        <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="پسورد" required data-parsley-minlength="6">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <div class="checkbox-styled checkbox-inline">
                            <input name="remmember" id="remmember" type="checkbox">
                            <label for="remmember">یادآوری من!!!</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-default btn-lg">ورود</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--Scripts-->
<!--JQuery-->
<script type="text/javascript" src="/assets/admin/js/vendors/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/assets/admin/js/vendors/jquery/jquery-ui.min.js"></script>
<!--Parsley-->
<script type="text/javascript" src="/assets/admin/js/vendors/parsley/parsley-config.js"></script>
<script type="text/javascript" src="/assets/admin/js/vendors/parsley/parsley.min.js"></script>
<!--Main App-->
<script type="text/javascript" src="/assets/admin/js/scripts.js"></script>
<!--/Scripts-->
</body>


<meta http-equiv="content-type" content="text/html;charset=windows-1251">
</html>
