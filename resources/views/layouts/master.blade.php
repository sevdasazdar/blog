<!DOCTYPE html>
<html style="font-size: 16px;" lang="fa"><head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="جراحی بینی،عمل بینیف دکتر علیرضا علمداریوبهترین جراح بینی">
    <meta name="description" content="جراحی زیبایی بینی و عمل گوش برجسته و نیز اندوسکوپی سینوس از خدمات دکتر علمداری می باشد. شما در این سایت میتوانید تغییرات حاصل از عمل زیبایی بینی و کلیه خدمات دیگر دکتر علیرضا کیفری علمداری را مشاهده نمایید">
    <meta name="robots" content="index,follow">
    <meta name="google-site-verification" content="hGziCToxpCrFKy7ka6VS_OYu2sz7oC49-xjF7SEQ84I" />
    <title>{{$pageTitle ?? ''}}</title>
    <link rel="stylesheet" href="/css/app.css" media="screen">
    <link rel="stylesheet" href="/css/{{$style}}" media="screen">
    <link rel="stylesheet" href="/css/bootstrap.min.css" media="screen">

    <script class="u-script" type="text/javascript" src="/js/jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="/js/bootstrap.min.js" defer=""></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script class="u-script" type="text/javascript" src="/js/app.js" defer=""></script>
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700">
    
    <script type="application/ld+json">{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": "",
		"logo": "/images/default-logo.png"
    }
    </script>
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Home">
    <meta property="og:type" content="website">
  </head>
  <body class="u-body u-xl-mode" data-lang="en">
       @include('layouts.header')
       @yield('content')
    
    
       @yield('extraScripts')
    @include('layouts.footer')

</body>
</html>