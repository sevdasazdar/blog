<footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-e6be">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="col-md-12 foterSection">
            <div class="col-md-4 foterContact">
                <div class="col-md-12 titleSection">
                    <h5>تلفنهای تماس </h5>
                </div>
                <div class="col-md-12">
                    <ol>
                        <li>
                            <p> 021-88202944 </p>
                        </li>
                        <li>
                            <p> 021-88202945 </p>
                        </li>
                        <li>
                            <p> 09021313153 </p>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="col-md-4 foterContact">
                <div class="col-md-12 titleSection">
                    <h5>  آدرس و ساعات کاری</h5>
                </div>
                <div class="col-md-12">
                    <ol>
                        <li class="timeTitle">
                            <h6 >ساعات کاری مطب :</h6>
                        </li>
                        <li style="list-style-type: none">
                            <p>یکشنبه و چهارشنبه ها از ساعت 4</p>
                        </li>
                        <li class="addressTitle">
                            <h6 > آدرس :</h6>
                        </li>
                        <li style="list-style-type: none">
                        <p>تهران،میدان ونک، اول خیابان صانعی، پلاک۵۰، واحد ۶</p>
                        </li>
                    </ol>
                </div>


            </div>

            <div class="col-md-4 foterContact">
                <div class="col-md-12 titleSection">
                    <h5>  لینکهای مفید </h5>
                </div>
                <div class="col-md-12">
                    <ol>
                        <li style="list-style-type: none">
                            <a href="{{ route('home') }}"> صفحه اصلی</a>
                        </li>
                        <li style="list-style-type: none">
                            <a href="{{ route('about') }}"> درباره دکتر</a>
                        </li>
                        <li style="list-style-type: none">
                            <a href="{{ route('images') }}"> عکسها</a>
                        </li>
                        <li style="list-style-type: none">
                            <a href="{{ route('videos') }}"> ویدیوها</a>
                        </li>
                    </ol>
                </div>


            </div>
        </div>

    </div>
    <section class="u-backlink u-clearfix u-grey-80">
        {{-- <a class="u-link" href="#" target="_blank">
            <span>Copyright©</span>
        </a> --}}
        <p class="u-text">
            Copyright  <span class="copyRight">©</span>dr alamdary</span>
        </p>
        {{-- <a class="u-link" href="" target="_blank">
            <span>dr alamdary</span>
        </a>. --}}
    </section>
</footer>
