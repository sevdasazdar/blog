@extends('layouts.master')
@section('content')
    <section class="u-clearfix u-image u-section" id="sec-ae35" data-image-width="1620" data-image-height="1080">
        <div class="u-clearfix u-sheet u-sheet">
            <div class="u-clearfix u-gutter-22 u-layout-wrap u-layout-wrap">
                <div class="u-layout">
                    <div class="u-layout-row">
                        <div class="u-size-60">
                            <div class="u-layout-col">
                                <div class="u-size-30">
                                    @foreach($images as $key=>$item)
                                    <div class="u-layout-row">
                                        <div class="u-align-left u-container-style u-layout-cell u-size-30 u-layout-cell">
                                            <div class="u-container-layout u-container-layout">
                                                <div class="u-carousel u-gallery u-gallery-slider u-layout-carousel u-lightbox u-no-transition u-show-text-on-hover u-gallery"
                                                    id="carousel-6a2{{$key}}" data-interval="5000" data-u-ride="carousel">
                                                    <ol
                                                        class="u-absolute-hcenter u-carousel-indicators u-carousel-indicators">
                                                        <li data-u-target="#carousel-6a2{{$key}}" data-u-slide-to="0"
                                                            class="u-active u-grey-70 u-shape-circle"
                                                            style="width: 10px; height: 10px;"></li>
                                                        <li data-u-target="#carousel-6a2{{$key}}" data-u-slide-to="1"
                                                            class="u-grey-70 u-shape-circle"
                                                            style="width: 10px; height: 10px;"></li>
                                                    </ol>
                                                    <div class="u-carousel-inner u-gallery-inner" role="listbox">
                                                        <div
                                                            class="u-active u-carousel-item u-effect-fade u-gallery-item u-carousel-item">
                                                            <div class="u-back-slide">
                                                                <img class="u-back-image u-expanded"
                                                                    src="{{asset('storage/'.$item->tamamrokh)}}" alt="عکس عمل {{$item->type}} از تمام رخ">
                                                            </div>
                                                            {{-- <div
                                                                class="u-align-center u-over-slide u-shading u-valign-bottom u-over-slide">
                                                                <h3 class="u-gallery-heading">Sample Title</h3>
                                                                <p class="u-gallery-text">Sample Text</p>
                                                            </div> --}}
                                                        </div>
                                                        <div
                                                            class="u-carousel-item u-effect-fade u-gallery-item u-carousel-item">
                                                            <div class="u-back-slide">
                                                                <img class="u-back-image u-expanded"
                                                                    src="{{asset('storage/'.$item->nimrokh)}}" alt="عکس عمل {{$item->type}} از نیم رخ">
                                                            </div>
                                                            {{-- <div
                                                                class="u-align-center u-over-slide u-shading u-valign-bottom u-over-slide">
                                                                <h3 class="u-gallery-heading">Sample Title</h3>
                                                                <p class="u-gallery-text">Sample Text</p>
                                                            </div> --}}
                                                        </div>
                                                    </div>
                                                    <a class="u-absolute-vcenter u-carousel-control u-carousel-control-prev u-grey-70 u-icon-circle u-opacity u-opacity-70 u-spacing-10 u-text-white u-carousel-control"
                                                        href="#carousel-6a2{{$key}}" role="button" data-u-slide="prev">
                                                        <span aria-hidden="true">
                                                            <svg viewBox="0 0 451.847 451.847">
                                                                <path
                                                                    d="M97.141,225.92c0-8.095,3.091-16.192,9.259-22.366L300.689,9.27c12.359-12.359,32.397-12.359,44.751,0
                                                                    c12.354,12.354,12.354,32.388,0,44.748L173.525,225.92l171.903,171.909c12.354,12.354,12.354,32.391,0,44.744
                                                                    c-12.354,12.365-32.386,12.365-44.745,0l-194.29-194.281C100.226,242.115,97.141,234.018,97.141,225.92z">
                                                                </path>
                                                            </svg>
                                                        </span>
                                                        <span class="sr-only">
                                                            <svg viewBox="0 0 451.847 451.847">
                                                                <path
                                                                    d="M97.141,225.92c0-8.095,3.091-16.192,9.259-22.366L300.689,9.27c12.359-12.359,32.397-12.359,44.751,0
                                                                                c12.354,12.354,12.354,32.388,0,44.748L173.525,225.92l171.903,171.909c12.354,12.354,12.354,32.391,0,44.744
                                                                                c-12.354,12.365-32.386,12.365-44.745,0l-194.29-194.281C100.226,242.115,97.141,234.018,97.141,225.92z">
                                                                </path>
                                                            </svg>
                                                        </span>
                                                    </a>
                                                    <a class="u-absolute-vcenter u-carousel-control u-carousel-control-next u-grey-70 u-icon-circle u-opacity u-opacity-70 u-spacing-10 u-text-white u-carousel-control-1"
                                                        href="#carousel-6a2{{$key}}" role="button" data-u-slide="next">
                                                        <span aria-hidden="true">
                                                            <svg viewBox="0 0 451.846 451.847">
                                                                <path
                                                                    d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
                                                                            L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
                                                                            c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z">
                                                                </path>
                                                            </svg>
                                                        </span>
                                                        <span class="sr-only">
                                                            <svg viewBox="0 0 451.846 451.847">
                                                                <path
                                                                    d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
                                                                        L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
                                                                        c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z">
                                                                </path>
                                                            </svg>
                                                        </span>
                                                    </a>
                                                </div>
                                                <p class="u-text u-text-default u-text">{!!$item->date!!}</p>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    @endforeach
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
