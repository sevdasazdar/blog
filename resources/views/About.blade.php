@extends('layouts.master')
@section('content')
<!DOCTYPE html>
<section class="u-clearfix u-section-1" id="sec-2af7">
  <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
    <div class="u-clearfix u-expanded-width-md u-expanded-width-sm u-expanded-width-xs u-gutter-0 u-layout-wrap u-layout-wrap-1">
      <div class="u-layout">
        <div class="u-layout-row">
          <div class="u-container-style u-image u-layout-cell u-left-cell u-size-30 u-image-1" src="" data-image-width="256" data-image-height="256">
            <div class="u-container-layout u-container-layout-1"></div>
          </div>
          <div class="u-align-left-xs u-container-style u-layout-cell u-right-cell u-size-30 u-layout-cell-2">
            <div class="u-container-layout u-container-layout-2">
              <div class="u-container-style u-expanded-width-xs u-group u-white u-group-1">
                <div class="u-container-layout u-valign-top u-container-layout-3">
                  <h2 class="u-text u-text-default u-text-1"> دکترعلمداری </h2>
                  <p class="u-text u-text-2">دکتر علیرضا کیفری علمداری ،
                    فلوشیپ جراحی زیبایی بینی و اندوسکوپی سینوس و متخصص گوش حلق و بینی
                    با ۲۰ سال تجربه در انجام عمل‌های جراحی بینی در زمینه‌های زیبایی، جراحی مجدد بینی، انحرافات مادرزادی، صدمات، درمان سپتوم منحرف، درمان شاخک‌های بزرگ بینی و پولیپ یکی از متبحرترین پزشکان در این حیطه بوده و آماده ارائه با کیفیت‌ترین خدمات به مراجعه‌کنندگان است.
                  </p>
                  {{-- <a href="" class="u-active-none u-border-2 u-border-no-left u-border-no-right u-border-no-top u-border-palette-1-base u-btn u-btn-rectangle u-button-style u-hover-none u-none u-btn-1">Hyperlink</a> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection