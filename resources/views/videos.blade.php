@extends('layouts.master')
@section('content')
    @foreach ($videos as $item)
        <section class="u-clearfix u-section-1" id="sec-ff96">
            <div class="u-clearfix u-sheet u-sheet-1">
                <div class="u-clearfix u-expanded-width u-gutter-50 u-layout-wrap u-layout-wrap-1">
                    <div class="u-layout">
                        <div class="u-layout-row">
                            <div class="u-align-left u-container-style u-layout-cell u-left-cell u-size-30 u-layout-cell-1">
                                <div class="u-container-layout u-container-layout-1 u-container-layout-videos">
                                    <div class="u-expanded u-video">
                                        <div class="embed-responsive embed-responsive-1">
                                            <video width="800" height="auto" class="mediaPlayer" controls
                                                style="padding: 200px">
                                                <source src="{{ asset('storage/'.$item->video)}}" type="video/mp4">
                                                <source src="{{  asset('storage/'.$item->video) }}" type="video/ogg">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="u-align-left u-container-style u-image u-layout-cell u-right-cell u-size-30 "
                                src="" data-image-width="1920" data-image-height="1080">
                                <img class="u-container-layout u-container-layout-2" src="{{ asset('storage/'.$item->image) }}"
                                    alt="عکس جراحی بینی" style="width:100%;height:480px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endforeach
@endsection
