<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->


<url>
  <loc>http://dr-alamdary.ir/</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>1.00</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/about</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/contact</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/videos</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/image</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/image?type=nose</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/image?type=other</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/image?type=ear</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/videos?type=other</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/videos?type=ear</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://dr-alamdary.ir/videos?type=nose</loc>
  <lastmod>2022-12-25T20:39:04+00:00</lastmod>
  <priority>0.80</priority>
</url>


</urlset>