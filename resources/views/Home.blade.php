@extends('layouts.master')
@section('content')
    <section class="u-align-left u-clearfix u-image u-section-1" id="carousel_d25c">
        <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
            <h1 class="u-custom-font u-font-oswald u-text u-text-palette-2-dark-2 u-title u-text-1">دکتر علیرضا علمداری</h1>
            <p class="u-text u-text-2">دکتر علیرضا کیفری علمداری ، فلوشیپ جراحی زیبایی بینی و اندوسکوپی سینوس و متخصص گوش حلق
                و بینی
            </p>
            <a href="{{ route('about') }}" class="u-btn u-button-style u-palette-2-base u-btn-1">مشاهده بیشتر</a>
        </div>
    </section>
    <section class="u-align-center u-clearfix u-section-2" id="carousel_c4da">
        <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
            <div class="u-clearfix u-expanded-width u-gutter-0 u-layout-wrap u-layout-wrap-1">
                <div class="u-gutter-0 u-layout">
                    <div class="u-layout-row">
                        <div class="u-size-20 u-size-30-md">
                            <div class="u-layout-col">
                                <div
                                    class="u-align-center u-container-style u-layout-cell u-shape-rectangle u-size-30 u-layout-cell-1">
                                    <div class="u-container-layout u-valign-middle u-container-layout-1"><span
                                            class="u-file-icon u-icon u-radius-50 u-icon-1"><img src="/images/3427866.png"
                                                alt="عکسهای عمل بینی"></span>
                                        <h4 class="u-text u-text-1"><a
                                                href="{{ route('images', ['type' => 'nose']) }}">تصاویر
                                                مربوط به جراحی بینی</a></h4>
                                        {{-- <p class="u-custom-font u-heading-font u-text u-text-2">Sample text. Click to select --}}
                                        {{-- the text box. Click again or double click to start editing the text.</p> --}}
                                    </div>
                                </div>
                                <div
                                    class="u-align-center u-container-style u-grey-5 u-layout-cell u-shape-rectangle u-size-30 u-layout-cell-2">
                                    <div class="u-container-layout u-valign-middle u-container-layout-2"><span
                                            class="u-file-icon u-icon u-radius-50 u-icon-2"><img src="/images/8298570.png"
                                                alt=""></span>
                                        <h4 class="u-text u-text-3"><a href="{{ route('images', ['type' => 'other']) }}">
                                                تصاویر مربوط به دیگر موارد</a></h4>
                                        {{-- <p class="u-custom-font u-heading-font u-text u-text-4">Sample text. Click to select
                                            the text box. Click again or double click to start editing the text.</p> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="u-size-20 u-size-30-md">
                            <div class="u-layout-col">
                                <div class="u-align-left u-container-style u-image u-layout-cell u-size-60 u-image-1"
                                    data-image-width="1000" data-image-height="1500">
                                    <div class="u-container-layout u-container-layout-3"></div>
                                </div>
                            </div>
                        </div>
                        <div class="u-size-20 u-size-60-md">
                            <div class="u-layout-col">
                                <div
                                    class="u-align-center u-container-style u-grey-5 u-layout-cell u-shape-rectangle u-size-30 u-layout-cell-4">
                                    <div class="u-container-layout u-valign-middle u-container-layout-4"><span
                                            class="u-file-icon u-icon u-icon-3"><img src="/images/3209068.png"
                                                alt=""></span>
                                        <h4 class="u-text u-text-5"><a
                                                href="{{ route('images', ['type' => 'ear']) }}">تصاویر
                                                مربوط به جراحی گوش</a></h4>
                                        {{-- <p class="u-custom-font u-heading-font u-text u-text-6">Sample text. Click to select
                                            the text box. Click again or double click to start editing the text.</p> --}}
                                    </div>
                                </div>
                                <div
                                    class="u-align-center u-container-style u-layout-cell u-shape-rectangle u-size-30 u-layout-cell-5">
                                    <div class="u-container-layout u-valign-middle u-container-layout-5">
                                        <span class="u-icon u-icon-circle u-radius-50 u-text-black u-icon-4">
                                            <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice"
                                                viewBox="0 0 512 512" style="">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-db2d">
                                                </use>
                                            </svg>
                                            <svg class="u-svg-content" viewBox="0 0 512 512" id="svg-db2d">
                                                <g>
                                                    <path
                                                        d="m164.593 162.777-86.833 86.833c-5.858 5.858-5.858 15.355 0 21.213l78.4 78.4c5.857 5.858 15.355 5.859 21.213 0 5.858-5.858 5.858-15.355 0-21.213l-67.793-67.794 76.226-76.227c5.858-5.858 5.858-15.355 0-21.213s-15.356-5.858-21.213.001z">
                                                    </path>
                                                    <path
                                                        d="m355.84 162.777c-5.857-5.858-15.355-5.858-21.213 0s-5.858 15.355 0 21.213l67.793 67.794-76.226 76.227c-5.858 5.858-5.858 15.355 0 21.213 5.857 5.858 15.355 5.859 21.213 0l86.833-86.833c5.858-5.858 5.858-15.355 0-21.213z">
                                                    </path>
                                                    <path
                                                        d="m300.389 129.558c-7.814-2.76-16.38 1.338-19.139 9.149l-79.318 224.597c-2.758 7.812 1.337 16.38 9.149 19.139 7.809 2.758 16.38-1.336 19.139-9.149l79.318-224.597c2.758-7.812-1.338-16.381-9.149-19.139z">
                                                    </path>
                                                    <path
                                                        d="m439.536 68.701h-367.072c-39.957 0-72.464 32.507-72.464 72.464v229.67c0 39.957 32.507 72.464 72.464 72.464h367.072c39.957 0 72.464-32.507 72.464-72.464v-229.67c0-39.957-32.507-72.464-72.464-72.464zm42.464 302.134c0 23.415-19.049 42.464-42.464 42.464h-367.072c-23.415 0-42.464-19.049-42.464-42.464v-229.67c0-23.415 19.049-42.464 42.464-42.464h367.072c23.415 0 42.464 19.049 42.464 42.464z">
                                                    </path>
                                                </g>
                                            </svg>
                                        </span>
                                        <h4 class="u-text u-text-7"><a href="{{ route('images') }}">همه تصاویر</a> </h4>
                                        {{-- <p class="u-custom-font u-heading-font u-text u-text-8">Sample text. Click to select
                                            the text box. Click again or double click to start editing the text.</p> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="u-align-left u-clearfix u-white u-section-3" id="carousel_1560">
        <div class="u-clearfix u-sheet u-sheet-1">
            <img class="u-image u-image-1" src="/images/696115194-0.jpeg" data-image-width="5760" data-image-height="3840">
            <div class="u-expanded-width-sm u-list u-list-1">
                <div class="u-repeater u-repeater-1">
                    <div
                        class="u-align-center u-container-style u-list-item u-palette-1-base u-repeater-item u-list-item-1">
                        <div class="u-container-layout u-similar-container u-valign-top u-container-layout-1">
                            <span class="u-align-left u-icon u-icon-circle u-text-palette-1-base u-white u-icon-1">
                                <svg class="u-svg-link u-svg-black" preserveAspectRatio="xMidYMin slice"
                                    viewBox="0 0 512 512" style="">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-db2d">
                                    </use>
                                </svg>
                                <svg class="u-svg-content" viewBox="0 0 512 512" id="svg-db2d">
                                    <g>
                                        <path
                                            d="m164.593 162.777-86.833 86.833c-5.858 5.858-5.858 15.355 0 21.213l78.4 78.4c5.857 5.858 15.355 5.859 21.213 0 5.858-5.858 5.858-15.355 0-21.213l-67.793-67.794 76.226-76.227c5.858-5.858 5.858-15.355 0-21.213s-15.356-5.858-21.213.001z">
                                        </path>
                                        <path
                                            d="m355.84 162.777c-5.857-5.858-15.355-5.858-21.213 0s-5.858 15.355 0 21.213l67.793 67.794-76.226 76.227c-5.858 5.858-5.858 15.355 0 21.213 5.857 5.858 15.355 5.859 21.213 0l86.833-86.833c5.858-5.858 5.858-15.355 0-21.213z">
                                        </path>
                                        <path
                                            d="m300.389 129.558c-7.814-2.76-16.38 1.338-19.139 9.149l-79.318 224.597c-2.758 7.812 1.337 16.38 9.149 19.139 7.809 2.758 16.38-1.336 19.139-9.149l79.318-224.597c2.758-7.812-1.338-16.381-9.149-19.139z">
                                        </path>
                                        <path
                                            d="m439.536 68.701h-367.072c-39.957 0-72.464 32.507-72.464 72.464v229.67c0 39.957 32.507 72.464 72.464 72.464h367.072c39.957 0 72.464-32.507 72.464-72.464v-229.67c0-39.957-32.507-72.464-72.464-72.464zm42.464 302.134c0 23.415-19.049 42.464-42.464 42.464h-367.072c-23.415 0-42.464-19.049-42.464-42.464v-229.67c0-23.415 19.049-42.464 42.464-42.464h367.072c23.415 0 42.464 19.049 42.464 42.464z">
                                        </path>
                                    </g>
                                </svg>
                            </span>
                            <h5 class="u-text u-text-1"><a href="{{ route('videos') }}">همه ویدیوها</a>
                            </h5>
                            <p class="u-text u-text-2">لطفا برای مشاهده تمام ویدیوها کیلیک کنید</p>
                        </div>
                    </div>
                    <div
                        class="u-align-center u-container-style u-list-item u-palette-1-base u-repeater-item u-list-item-2">
                        <div class="u-container-layout u-similar-container u-valign-top u-container-layout-2">
                            <span class="u-align-left u-file-icon u-icon u-icon-circle u-white u-icon-2"><img
                                    src="/images/8298570.png" alt=""></span>
                            <h5 class="u-text u-text-3"><a href="{{ route('videos', ['type' => 'other']) }}">دیگر
                                    ویدیوها</a></h5>
                            <p class="u-text u-text-4">لطفا برای مشاهده ویدیوهای مربوط به عمل های غیر از بینی و گوش کیلیک کنید</p>
                        </div>
                    </div>

                    <div
                        class="u-align-center u-container-style u-list-item u-palette-1-base u-repeater-item u-list-item-4">
                        <div class="u-container-layout u-similar-container u-valign-top u-container-layout-4"><span
                                class="u-align-left u-file-icon u-icon u-icon-circle u-white u-icon-4"><img
                                    src="/images/3209068.png" alt="ویدیوهای جراحی گوش"></span>
                            <h5 class="u-text u-text-7"><a href="{{ route('videos', ['type' => 'ear']) }}">ویدیوهای مربوط
                                    به جراحی گوش</a></h5>
                            <p class="u-text u-text-8">لطفا برای مشاهده ویدیوهای مربوط به عمل گوش کیلیک کنید</p>
                        </div>
                    </div>
                    <div
                        class="u-align-center u-container-style u-list-item u-palette-1-base u-repeater-item u-list-item-3">
                        <div class="u-container-layout u-similar-container u-valign-top u-container-layout-3"><span
                                class="u-align-left u-file-icon u-icon u-icon-circle u-white u-icon-3"><img
                                    src="/images/3427866.png" alt="ویدیوهای عمل بینی"></span>
                            <h5 class="u-text u-text-5"><a href="{{ route('videos', ['type' => 'nose']) }}">ویدیوهای مربوط
                                    به جراحی بینی</a></h5>
                            <p class="u-text u-text-6">لطفا برای مشاهده ویدیوهای مربوط به عمل بینی کیلیک کنید</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="u-clearfix u-section-4" id="carousel_0138">
        <div class="u-clearfix u-sheet u-sheet-1">
            <div class="u-clearfix u-expanded-width u-gutter-24 u-layout-wrap u-layout-wrap-1">
                <div class="u-gutter-0 u-layout">
                    <div class="u-layout-row">
                        <div class="u-size-30">
                            <div class="u-layout-col">
                                <div class="u-container-style u-image u-layout-cell u-size-60 u-image-1"
                                    data-image-width="719" data-image-height="1080">
                                    <div class="u-container-layout u-container-layout-1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="u-size-30">
                            <div class="u-layout-col">
                                <div class="u-align-left u-container-style u-layout-cell u-size-20 u-layout-cell-2">
                                    <div class="u-container-layout u-valign-middle u-container-layout-2">
                                        <h1 class="u-custom-font u-font-oswald u-text u-text-1">توصیه های بعد عمل</h1>
                                    </div>
                                </div>
                                <div class="u-container-style u-layout-cell u-size-20 u-white u-layout-cell-3">
                                    <div class="u-container-layout u-valign-middle u-container-layout-3">
                                        {{-- <p class="u-text u-text-2"> --}}
                                        <ol class=" u-text u-text-2 afrerSurgery">
                                            <li>کمپرس سرد به مدت 48 ساعت</li>
                                            <li>کمپرس گرم بعد از روز دوم به مدت 5 روز</li>
                                            <li>موقع خواب نیمتنه فوقانی 30 درجه بالا باشد ولی خواب به سمت راست یا چپ مشکلی
                                                ندارد</li>
                                            <li>کارهای سبک بعد از روز اول مشکلی ندارد ولی کارهای سنگین بعد از یک ماه انجام
                                                شود</li>
                                            <li>عینک یک ماه بعد در صورتی که ضروری باشد که روش آن را دکتر توضیح می دهد</li>
                                            <li>کپسول سفالکسین هر 6 ساعت</li>
                                            <li>قرص استامینوفن در صورت درد</li>
                                            <li>برای شستشوی بینی از اسپری دکوسالین هر شش ساعت دو پاف در هر دو بینی یا از سرم
                                                نرمال سالین 5 سی سی در هر طرف بینی استفاده کنید</li>
                                            <li>بعد از شستشوی بینی از قطره فنیل آفرین سه قطره در هر طرف بینی استفاده کنید.
                                            </li>
                                            <li>رژیم غذایی روز اول مایعات، از روز دوم معمولی</li>
                                            <li>روزی که برای برداشتن اسپلینت و بخیه ها مراجعه میکنید،حتما خوب دوش بگیرید تا
                                                اسپلینت شل شود</li>
                                            <li>چسب تا یک ماه لازم است</li>
                                            <li>بینی با پوست تازک تا یک سال و بینیهای با پوست ضخیم تا دو سه سال ورم دارند
                                            </li>
                                            <li>تا یکی دو ماه، گاها تا شش ماه گرفتگی بینی طبیعی است.از قطره و یا اسپری بینی
                                                بتامتازون در صورت وجود مشکل استفاده کنید</li>
                                            <li>پایین بودن سر موقع کار اشکال ندارد.</li>
                                        </ol>
                                        {{-- </p> --}}
                                        <button id="more" class="u-btn u-button-style u-palette-3-base u-btn-1">
                                            مشاهده بیشتر</button>
                                    </div>
                                </div>
                                <div class="u-container-style u-layout-cell u-size-20 u-layout-cell-4">
                                    <div class="u-container-layout u-container-layout-4">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('extraScripts')
    <script>
        $("#more").on('click', function(e) {
            var height = $(".afrerSurgery").height();
            if (height == "150") {
                $('.afrerSurgery').css('height', "auto");
            } else {
                $('.afrerSurgery').css('height', "150px");
            }
            e.preventDefault();

        })
    </script>
@endsection
