@extends('layouts.master')
@section('content')
<section class="u-align-center u-clearfix u-palette-1-base u-section-1" id="sec-ce55">
  <div class="u-clearfix u-sheet u-valign-middle-lg u-valign-middle-md u-valign-middle-xl u-sheet-1">
    {{-- <h2 class="u-text u-text-default u-text-palette-4-base u-text-1"> Get in touch</h2> --}}
    <div class="u-list u-list-1">
      <div class="u-repeater u-repeater-1">
        <div class="u-align-center u-border-3 u-border-white u-container-style u-hover-feature u-list-item u-radius-20 u-repeater-item u-shape-round u-list-item-1">
          <div class="u-container-layout u-similar-container u-valign-middle u-container-layout-1"><span class="u-hover-feature u-icon u-icon-rectangle u-text-white u-icon-1"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 54.757 54.757" style="">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-487b"></use>
              </svg><svg class="u-svg-content" viewBox="0 0 54.757 54.757" x="0px" y="0px" id="svg-487b" style="enable-background:new 0 0 54.757 54.757;">
                <path d="M40.94,5.617C37.318,1.995,32.502,0,27.38,0c-5.123,0-9.938,1.995-13.56,5.617c-6.703,6.702-7.536,19.312-1.804,26.952
	L27.38,54.757L42.721,32.6C48.476,24.929,47.643,12.319,40.94,5.617z M27.557,26c-3.859,0-7-3.141-7-7s3.141-7,7-7s7,3.141,7,7
	S31.416,26,27.557,26z"></path>
              </svg></span>
            <h6 class="u-text u-text-default u-text-2">تهران،ونک،اول خیابان صانعی،پلاک 50 واحد 6</h6>
          </div>
        </div>
        <div class="u-align-center u-border-3 u-border-white u-container-style u-hover-feature u-list-item u-radius-20 u-repeater-item u-shape-round u-list-item-2">
          <div class="u-container-layout u-similar-container u-valign-middle u-container-layout-2"><span class="u-file-icon u-hover-feature u-icon u-icon-rectangle u-text-white u-icon-2"><img src="/images/3059446-817ccdce.png" alt=""></span>
            <h6 class="u-text u-text-default u-text-3">
              <a href="tel:09021313153" class="u-active-none u-border-1 u-border-active-palette-4-light-2 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-border-white u-bottom-left-radius-0 u-bottom-right-radius-0 u-btn u-button-link u-button-style u-hover-none u-none u-radius-0 u-text-active-palette-4-light-2 u-text-body-alt-color u-text-hover-palette-4-light-2 u-top-left-radius-0 u-top-right-radius-0 u-btn-1"> 09021313153</a>
            </h6>
          </div>
        </div>
        <div class="u-align-center u-border-3 u-border-white u-container-style u-hover-feature u-list-item u-radius-20 u-repeater-item u-shape-round u-list-item-3">
          <div class="u-container-layout u-similar-container u-valign-middle u-container-layout-3"><span class="u-file-icon u-hover-feature u-icon u-icon-rectangle u-text-white u-icon-3"><img src="images/instagram.png" alt=""></span>
            <h6 class="u-text u-text-default u-text-4">
              <a href="https://www.instagram.com/dr_alamdary" target="blanck" class="u-active-none u-border-1 u-border-active-palette-4-light-2 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-border-white u-btn u-button-link u-button-style u-hover-none u-none u-text-active-palette-4-light-2 u-text-body-alt-color u-text-hover-palette-4-light-2 u-btn-2" data-animation-name="" data-animation-duration="0" data-animation-delay="0" data-animation-direction="">dr_alamdary</a>
            </h6>
          </div>
        </div>
      </div>
    </div>
    <div class="u-align-center u-container-style u-expanded-width-sm u-expanded-width-xs u-group u-radius-20 u-shape-round u-white u-group-1">
      <div class="u-container-layout u-valign-middle-lg u-valign-middle-md u-valign-middle-sm u-valign-middle-xl u-container-layout-4">
        <div class="u-form u-form-1">
          <form action="" class="u-clearfix u-form-spacing-15 u-form-vertical u-inner-form" source="email" name="form" style="padding: 0px;">
            <div class="u-form-group u-form-name">
              <label for="name-4c18" class="u-label u-label-1">Name</label>
              <input type="text" placeholder="Enter your Name" id="name-4c18" name="name" class="u-grey-10 u-input u-input-rectangle u-radius-10 u-input-1" required="">
            </div>
            <div class="u-form-email u-form-group">
              <label for="email-4c18" class="u-label u-label-2">Email</label>
              <input type="email" placeholder="Enter a valid email address" id="email-4c18" name="email" class="u-grey-10 u-input u-input-rectangle u-radius-10 u-input-2" required="">
            </div>
            <div class="u-form-group u-form-message">
              <label for="message-4c18" class="u-label u-label-3">Message</label>
              <textarea placeholder="Enter your message" rows="4" cols="50" id="message-4c18" name="message" class="u-grey-10 u-input u-input-rectangle u-radius-10 u-input-3" required=""></textarea>
            </div>
            <div class="u-form-agree u-form-group u-form-group-4">
              <input type="checkbox" id="agree-a472" name="agree" class="u-agree-checkbox" required="">
              <label for="agree-a472" class="u-agree-label u-label u-label-4">I accept the <a href="#">Terms of Service</a>
              </label>
            </div>
            <div class="u-align-right u-form-group u-form-submit">
              <a href="#" class="u-active-palette-1-base u-border-active-palette-4-light-1 u-border-hover-palette-4-light-1 u-border-none u-btn u-btn-round u-btn-submit u-button-style u-hover-palette-1-base u-palette-4-base u-radius-10 u-btn-3">Submit</a>
              <input type="submit" value="submit" class="u-form-control-hidden">
            </div>
            <div class="u-form-send-message u-form-send-success"> Thank you! Your message has been sent. </div>
            <div class="u-form-send-error u-form-send-message"> Unable to send your message. Please fix errors then try again. </div>
            <input type="hidden" value="" name="recaptchaResponse">
            <input type="hidden" name="formServices" value="">
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection