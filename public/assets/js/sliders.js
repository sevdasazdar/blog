$(document).ready(function() {

    $('.videos .owl-carousel').owlCarousel({
        rtl: true,
        items: 3,
        lazyLoad: true,
        loop: true,
        margin: 25,
        nav:true,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            500:{
                items:2,
                nav:false,
            },
            600:{
                items:2,
                nav:false,
            },
            768:{
                items:3,
                nav:false,
                autoplay:true,
            },
            990:{
                items:3,
                nav:false,
                autoplay:true,

            },
            1200:{
                nav:false,
                autoplay:true,

            },
            1500:{
                autoplay:true,

            }
        }
    });
    $('.photos .owl-carousel').owlCarousel({
        rtl: true,
        items: 4,
        lazyLoad: true,
        loop: true,
        margin: 15,
        nav:true,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            500:{
                items:2,
                nav:false,
            },
            600:{
                items:2,
                nav:false,
            },
            768:{
                items:3,
                nav:false,
                autoplay:true,
            },
            990:{
                items:3,
                nav:false,
                autoplay:true,

            },
            1200:{
                nav:false,
                autoplay:true,

            },
            1500:{
                autoplay:true,

            }
        }
    });
    $('.images .owl-carousel').owlCarousel({
        rtl: true,
        items: 1,
        lazyLoad: true,
        loop: true,
        nav:true,
        margin: 15,
        dots: false,
        autoplay:true,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            500:{
                items:1,
                nav:false,
            },
            600:{
                items:1,
                nav:false,
            },
            768:{
                items:1,
                nav:false,
                autoplay:true,
            },
            990:{
                items:1,
                nav:false,
                autoplay:true,

            },
            1200:{
                items:1,
                nav:false,
                autoplay:true,

            },
            1500:{
                items:1,
                autoplay:true,

            }
        }
    });
    $('.news-subject .owl-carousel').owlCarousel({
        rtl: true,
        items: 1,
        lazyLoad: true,
        nav:true,
        loop: true,
        margin: 15,
        dots: false,
        autoplay:true,
        autoplayTimeout: 6000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            500:{
                items:1,
                nav:false,
            },
            600:{
                items:1,
                nav:false,
            },
            768:{
                items:1,
                nav:false,
                autoplay:true,
            },
            990:{
                items:1,
                nav:false,
                autoplay:true,

            },
            1200:{
                items:1,
                nav:false,
                autoplay:true,

            },
            1500:{
                items:1,
                autoplay:true,

            }
        }
    });

    $('.infographic .owl-carousel').owlCarousel({
        rtl: true,
        items: 1,
        lazyLoad: true,
        loop: true,
        margin: 25,
        nav:true,
        dots: true,
        autoplay:true,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            500:{
                items:2,
                nav:false,
            },
            600:{
                items:2,
                nav:false,
            },
            768:{
                items:3,
                nav:false,
                autoplay:true,
            },
            990:{
                items:3,
                nav:false,
                autoplay:true,

            },
            1200:{
                nav:false,
                autoplay:true,

            },
            1500:{
                autoplay:true,

            }
        }
    });





    $('.other_news .owl-carousel').owlCarousel({
        rtl: true,
        items: 3,
        lazyLoad: true,
        nav:true,
        loop: true,
        margin: 15,
        dots: false,
        autoplay:true,
        autoplayTimeout: 8000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            500:{
                items:1,
                nav:false,
            },
            600:{
                items:2,
                nav:false,
            },
            768:{
                items:3,
                nav:false,
                autoplay:true,
            },
            990:{
                items:3,
                nav:false,
                autoplay:true,

            },
            1200:{
                items:4,
                nav:false,
                autoplay:true,
            },
            1500:{
                items:3,
                autoplay:true,
            }
        }
    });

    $( ".owl-prev").html('<i class="fa fa-chevron-right"></i>');
    $( ".owl-next").html('<i class="fa fa-chevron-left"></i>');
});
