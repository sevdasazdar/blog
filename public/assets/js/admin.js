function changeProvience(){
    var provience=$(".provinceSearch").val();
    if(provience!=''){
        $.ajax({
            method: 'POST',
            url: "/api/findArea",
            data: {
                "provience": $(".provinceSearch").val(),
            },
            success: function (data) {
                $("#doctorsSection").empty();
                $.each(data,function(index,value){
                    $("#doctorsSection").append(`
                    <div class="col-md-6 drClass">
                        <div class="col-md-6">
                        <img class="picDr" src="${value.picture??''}" alt="">
                        </div>
                        <div class="col-md-6 drInfoSection">
                            <div class="drName">
                            <h6>${' دکتر'+' '+value.name+' '+value.lastname}</h6>
                            </div>
                            <div class="drspecialy">
                                <h6>${value.profesional.title}</h6>
                            </div>
                            <div class="drAddress">
                            <p>${value.address}</p>
                            </div>
        
                    </div>
                    </div>`);
                });
            }
        });
    }else{
        location.reload();
    }
}
function changeArea(){
    var area=$(".areaSearch").val();
    if(area!=''){
        $.ajax({
            method: 'POST',
            url: "/api/findArea",
            data: {
                "area": $(".areaSearch").val(),
            },
            success: function (data) {
                $("#doctorsSection").empty();
                $.each(data,function(index,value){
                    $("#doctorsSection").append(`
                    <div class="col-md-6 drClass">
                        <div class="col-md-6">
                        <img class="picDr" src="${value.picture??''}" alt="">
                        </div>
                        <div class="col-md-6 drInfoSection">
                             <div class="drName">
                             <h6>${' دکتر'+' '+value.name+' '+value.lastname}</h6>
                             </div>
                             <div class="drspecialy">
                                 <h6>${value.profesional.title}</h6>
                             </div>
                             <div class="drAddress">
                             <p>${value.address}</p>
                            </div>
         
                     </div>
                     </div>`);
                });
            }
        });
    }else{
        location.reload();
    }

}
function changeProfesional(){
    $(document).on('click', '.spTitle', function () {
        var sp =  $(this).text();
        var id=$(this).attr('property');
        if(sp!=''){
            $.ajax({
                method: 'POST',
                url: "/api/findArea",
                data: {
                    "profesional": sp,
                    "id":id
                },
                success: function (data) {
                    $("#doctorsSection").empty();
                    $.each(data,function(index,value){
                        $("#doctorsSection").append(`
                        <div class="col-md-6 drClass">
                            <div class="col-md-6">
                            <img class="picDr" src="${value.picture??''}" alt="">
                            </div>
                            <div class="col-md-6 drInfoSection">
                                 <div class="drName">
                                 <h6>${' دکتر'+' '+value.name+' '+value.lastname}</h6>
                                 </div>
                                 <div class="drspecialy">
                                     <h6>${value.profesional.title}</h6>
                                 </div>
                                 <div class="drAddress">
                                 <p>${value.address}</p>
                                </div>
             
                         </div>
                         </div>`);
                    });
                }
            });
        }else{
            location.reload();
        }
    });
}