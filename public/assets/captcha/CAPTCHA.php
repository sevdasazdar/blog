<?php
header('Content-Type: image/jpeg');
session_start();
$capCode =  rand(1000, 9999);
$_SESSION['CAPTCHA'] = $capCode;
$len = strlen($capCode);

$width = 400;
$height = 180;

$position = 5;
$space = $width / $len;
$img = ImageCreate($width + $position, $height);
$backColor = imagecolorallocate($img, 236, 238, 239);
$maskColor = imagecolorallocate($img, 127, 127, 127);


$fontsize = round($width / 5);

if(!isset($_GET['en'])){
    if(rand(1,2) == 1){
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $font = __DIR__ .'\Yekan.ttf';
        } else {
            $font = 'Yekan.ttf';
        }

    }else{
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $font = __DIR__ .'\bnikoo.ttf';
        } else {
            $font = 'bnikoo.ttf';
        }
    }
}else{
    if(rand(1,2) == 1){
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $font = __DIR__ .'\arial.ttf';
        } else {
            $font = 'arial.ttf';
        }

    }else{
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $font = __DIR__ .'\arial.ttf';
        } else {
            $font = 'arial.ttf';
        }
    }
}



for ($i = 0; $i < $len; $i++) {
    $rotate = rand(-20, 20);
    $foreColor = imagecolorallocate($img, rand(0, 150), rand(0, 150), rand(0, 150));
    imagettftext($img, $fontsize, $rotate, $position, 120, $foreColor, $font, substr($capCode, $i, 1));
    $position += $space;
}

ImageJpeg($img);

ImageDestroy($img);
exit;

