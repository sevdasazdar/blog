<?php

use App\Http\Controllers\admin\ImageController;
use App\Http\Controllers\admin\VideoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\Auth\LoginController;
use App\Http\Controllers\VideoController as HomeVideo;
use App\Http\Controllers\ImageController as HomeImage;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/about', [HomeController::class, 'about'])->name('about');
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');
Route::get('/image', [HomeImage::class, 'images'])->name('images');
Route::get('/videos', [HomeVideo::class, 'videos'])->name('videos');
Route::get('/sitemap.xml', [HomeController::class, 'sitemap'])->name('sitemap');
// Route::POST('test_mail', [HomeController::class, 'testMail'])->name('test_mail');


Route::prefix('admin')->group(function () {
    #Login
    Route::get('/login', [LoginController::class, 'show'])->name('admin.login');
    Route::post('/login', [LoginController::class, 'do']);
    #Logout
    Route::get('/logout', [LoginController::class, 'logout']);

    #Start AUTH
    Route::middleware('admin')->group(function () {
        #dashboard
        Route::get('/dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
        #video
        Route::resource('/videos', VideoController::class);
        #image
        Route::resource('/images', ImageController::class);
        #profile
        Route::get('/profile', [AdminController::class, 'profile']);
        #profile save
        Route::post('/doprofile', [AdminController::class, 'doprofile']);

        #profile password save
        Route::post('/password', [AdminController::class, 'password']);


        #####################################################################
    }
    );
});